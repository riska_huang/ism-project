/*
 * Interactive Shape Manipulation Homework
 * Feature Shape Deformation
 * StudentID: 0656643
 * Name: Riska Wilian Triany Putri - ���мz
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <vector>
#include <numeric>
#include <iostream>
#include <Windows.h>
#include <gl/GL.h>
#include <glut.h>

#include "glm.h"
#include "mtxlib.h"
#include "trackball.h"
#include <string>

using namespace std;

#define N 4;

_GLMmodel *mesh;
int WindWidth, WindHeight;

int last_x , last_y;
int selectedFeature = -1;
vector<int> featureList;
vector< vector<float> > pointDisList;
vector< vector<float> > rbfList;
vector<vector3> d;
vector<vector3>dChange;
vector<vector3> finalD;
vector<vector3> wValue;
vector<vector3> wvValue;
float rbfMat[100][100];
float dValue[100][100];
float rbfInv[100][100];

//Added Code
//Find |x-xk|
void PointDis()
{
	for (int i = 0; i < featureList.size(); i++)
	{
		int idx = featureList[i];
		vector3 p0(mesh->vertices[3 * idx + 0], mesh->vertices[3 * idx + 1], mesh->vertices[3 * idx + 2]);
		vector<float>pointRows;
		if (featureList.empty() == false)
		{
			for (int j = 0; j < featureList.size(); j++)
			{
				int jdx = featureList[j];
				vector3 p1(mesh->vertices[3 * jdx + 0], mesh->vertices[3 * jdx + 1], mesh->vertices[3 * jdx + 2]);
				/*float x = p1.x - p0.x;
				float y = p1.y - p0.y;
				float z = p1.z - p0.z;

				float dis = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));*/
				float dis = (p1 - p0).length();
				pointRows.push_back(dis);
				/*cout << "p0: " << p0.x <<", "<<p0.y << ", " <<p0.z << endl;
				cout << "p1: " << p1.x << ", " << p1.y << ", " << p1.z << endl;
				cout << "dis: " << dis << endl;*/
				//cout << "PointDisList: " << pointDisList[i] << endl;
			}
		}
		pointDisList.push_back(pointRows);
	}
	cout << "PointDisList: " << endl;
	for (const std::vector<float> &v : pointDisList)
	{
		for (float x : v) std::cout << x << ' ';
		std::cout << std::endl;
	}
}

//Find Average
float rAverage()
{
	float sum = 0;
	for (std::vector<std::vector<float>>::size_type i = 0; i < pointDisList.size(); i++)
	{
		for (std::vector<float>::size_type j = 0; j < pointDisList[i].size(); j++)
		{
			float pt = pointDisList[i][j];
			sum = sum + pt;
		}
	}
	float avg = sum / pointDisList.size();
	//cout << "Average: " << avg << endl;
	return avg;
	/*float px = 0;
	float py = 0;
	float pz = 0;
	for (int i = 0; i < featureList.size(); i++)
	{
	int idx = featureList[i];
	vector3 pt(mesh->vertices[3 * idx + 0], mesh->vertices[3 * idx + 1], mesh->vertices[3 * idx + 2]);

	px = px + pt.x;
	py = py + pt.y;
	pz = pz + pt.z;
	}
	vector3 avg(px / featureList.size(), py / featureList.size(), pz / featureList.size());
	return avg;*/
}

//Find Variance
float rVariance()
{
	float avg = rAverage();
	float sum = 0;
	for (std::vector<std::vector<float>>::size_type i = 0; i < pointDisList.size(); i++)
	{
		for (std::vector<float>::size_type j = 0; j < pointDisList[i].size(); j++)
		{
			float pt = pointDisList[i][j];
			float ptDiff = pow((pt - avg), 2);
			sum = sum + ptDiff;
		}
	}
	float variance = sum / pointDisList.size();
	//cout << "Variance: " << variance << endl;
	return variance;
	/*float sumMag = pow(mag1, 2) + pow(mag2, 2);
	float meanPow = pow(avg, 2);
	float variance = (sumMag / 2) - meanPow;
	return variance;*/
}

//calculate RBF
void RBF()
{
	float variance = rVariance();
	for (std::vector<std::vector<float>>::size_type i = 0; i < pointDisList.size(); i++)
	{
		vector<float>rbfRows;
		for (std::vector<float>::size_type j = 0; j < pointDisList[i].size(); j++)
		{
			float pt = pointDisList[i][j];
			float rbf = exp((-pow(pt, 2)) / (2 * variance));
			rbfRows.push_back(rbf);
		}
		rbfList.push_back(rbfRows);
	}
	cout << "RBFValue: " << endl;
	for (const std::vector<float> &v : rbfList)
	{
		for (float x : v) std::cout << x << ' ';
		std::cout << std::endl;
	}
}

//Convert Vector to Matrix
void VectorToMatrix()
{
	cout << "Convert RBF to Matrix: " << endl;
	for (vector<vector<float>>::size_type i = 0; i < rbfList.size(); i++)
	{
		for (vector<float>::size_type j = 0; j < rbfList[i].size(); j++)
		{
			rbfMat[i][j] = rbfList[i][j];
			cout << rbfMat[i][j] << ' ';
		}
		cout << endl;
	}

	cout << "Convert D Value to Matrix: " << endl;
	for (int i = 0; i < featureList.size(); i++)
	{
		int idx = featureList[i];
		vector3 pt(mesh->vertices[3 * idx + 0], mesh->vertices[3 * idx + 1], mesh->vertices[3 * idx + 2]);
		d.push_back(pt);
		for (int j = 0; j < 3; j++)
		{
			if (j == 0)
			{
				dValue[i][j] = pt.x;
			}
			else if (j == 1)
			{
				dValue[i][j] = pt.y;
			}
			else if (j == 2)
			{
				dValue[i][j] = pt.z;
			}
			cout << dValue[i][j] << ' ';
		}
		cout << endl;
	}
}

//	calculate minor of matrix OR build new matrix : k-had = minor
void minor(float b[100][100], float a[100][100], int i, int n){
	int j, l, h = 0, k = 0;
	for (l = 1; l<n; l++)
	for (j = 0; j<n; j++){
		if (j == i)
			continue;
		b[h][k] = a[l][j];
		k++;
		if (k == (n - 1)){
			h++;
			k = 0;
		}
	}
}// end function

//---------------------------------------------------
//	calculate determinte of matrix
float det(float a[100][100], int n){
	int i;
	float b[100][100], sum = 0;
	if (n == 1)
		return a[0][0];
	else if (n == 2)
		return (a[0][0] * a[1][1] - a[0][1] * a[1][0]);
	else
	for (i = 0; i<n; i++){
		minor(b, a, i, n);	// read function
		sum = (float)(sum + a[0][i] * pow(-1, i)*det(b, (n - 1)));	// read function	// sum = determinte matrix
	}
	return sum;
}// end function

//---------------------------------------------------
//	calculate transpose of matrix
void transpose(float c[100][100], float d[100][100], int n, float det){
	int i, j;
	float b[100][100];
	for (i = 0; i<n; i++)
	for (j = 0; j<n; j++)
		b[i][j] = c[j][i];
	for (i = 0; i<n; i++)
	for (j = 0; j<n; j++)
		d[i][j] = b[i][j] / det;	// array d[][] = inverse matrix
}// end function

//---------------------------------------------------
//	calculate cofactor of matrix
void cofactor(float a[100][100], float d[100][100], int n, float determinte){
	float b[100][100], c[100][100];
	int l, h, m, k, i, j;
	for (h = 0; h<n; h++)
	for (l = 0; l<n; l++){
		m = 0;
		k = 0;
		for (i = 0; i<n; i++)
		for (j = 0; j<n; j++)
		if (i != h && j != l){
			b[m][k] = a[i][j];
			if (k<(n - 2))
				k++;
			else{
				k = 0;
				m++;
			}
		}
		c[h][l] = (float)pow(-1, (h + l))*det(b, (n - 1));	// c = cofactor Matrix
	}
	transpose(c, d, n, determinte);	// read function
}// end function

//---------------------------------------------------
//	calculate inverse of matrix
void inverse(float a[100][100], float d[100][100], int n, float det){
	if (det == 0)
		cout << "Inverse of Entered Matrix is not possible" << endl;
	else if (n == 1)
		d[0][0] = 1;
	else
		cofactor(a, d, n, det);	// read function
}// end function

//Calculate the inverse of 
void InverseMatrix()
{
	float deter = det(rbfMat, rbfList.size());
	inverse(rbfMat, rbfInv, rbfList.size(), deter);
	cout << "The Inverse Of Matrix Is : " << endl;
	for (int i = 0; i<rbfList.size(); i++){
		for (int j = 0; j<rbfList.size(); j++)
			cout << rbfInv[i][j] << " ";
		cout << endl;
	}
}

//Calculate the weight of the selected point
void CalculateWeight()
{
	InverseMatrix();
	cout << "Calculate Weight: " << endl;
	for (int i = 0; i < rbfList.size(); i++)
	{
		//wValue[i] = {0,0,0};
		for (int j = 0; j < rbfList.size(); j++)
		{
			vector3 w = w + (d[i] *= rbfInv[i][j]);
			wValue.push_back(w);
		}
		cout << wValue[i].x << ' ' << wValue[i].y << ' ' << wValue[i].z << endl;
	}
}

void Reshape(int width, int height)
{
  int base = min(width , height);

  tbReshape(width, height);
  glViewport(0 , 0, width, height);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(45.0,(GLdouble)width / (GLdouble)height , 1.0, 128.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glTranslatef(0.0, 0.0, -3.5);

  WindWidth = width;
  WindHeight = height;
}

void Display(void)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glPushMatrix();
  tbMatrix();
  
  // render solid model
  glEnable(GL_LIGHTING);
  glColor3f(1.0 , 1.0 , 1.0f);
  glPolygonMode(GL_FRONT_AND_BACK , GL_FILL);
  glmDraw(mesh , GLM_SMOOTH);

  // render wire model
  glPolygonOffset(1.0 , 1.0);
  glEnable(GL_POLYGON_OFFSET_FILL);
  glLineWidth(1.0f);
  glColor3f(0.6 , 0.0 , 0.8);
  glPolygonMode(GL_FRONT_AND_BACK , GL_LINE);
  glmDraw(mesh , GLM_SMOOTH);

  // render features
  glPointSize(10.0);
  glColor3f(1.0 , 0.0 , 0.0);
  glDisable(GL_LIGHTING);
  glBegin(GL_POINTS);
	for (int i = 0 ; i < featureList.size() ; i++)
	{
		int idx = featureList[i];

		glVertex3fv((float *)&mesh->vertices[3 * idx]);
	}
  glEnd();
  
  glPopMatrix();

  glFlush();  
  glutSwapBuffers();
}

vector3 Unprojection(vector2 _2Dpos)
{
	float Depth;
	int viewport[4];
	double ModelViewMatrix[16];				//Model_view matrix
	double ProjectionMatrix[16];			//Projection matrix

	glPushMatrix();
	tbMatrix();

	glGetIntegerv(GL_VIEWPORT, viewport);
	glGetDoublev(GL_MODELVIEW_MATRIX, ModelViewMatrix);
	glGetDoublev(GL_PROJECTION_MATRIX, ProjectionMatrix);

	glPopMatrix();

	glReadPixels((int)_2Dpos.x , viewport[3] - (int)_2Dpos.y , 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &Depth);

	double X = _2Dpos.x;
	double Y = _2Dpos.y;
	double wpos[3] = {0.0 , 0.0 , 0.0};

	gluUnProject(X , ((double)viewport[3] - Y) , (double)Depth , ModelViewMatrix , ProjectionMatrix , viewport, &wpos[0] , &wpos[1] , &wpos[2]);

	return vector3(wpos[0] , wpos[1] , wpos[2]);
}

void mouse(int button, int state, int x, int y)
{
  tbMouse(button, state, x, y);

  // add feature
  if (button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN)
  {
	  int minIdx = 0;
	  float minDis = 9999999.0f;

	  vector3 pos = Unprojection(vector2((float)x , (float)y));

	  for (int i = 0 ; i < mesh->numvertices ; i++)
	  {
		  vector3 pt(mesh->vertices[3 * i + 0] , mesh->vertices[3 * i + 1] , mesh->vertices[3 * i + 2]);
		  float dis = (pos - pt).length();

		  if (minDis > dis)
		  {
			  minDis = dis;
			  minIdx = i;
		  }
	  }

	  featureList.push_back(minIdx);
  }

  // manipulate feature
  if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
  {
	  int minIdx = 0;
	  float minDis = 9999999.0f;
	  float dis;

	  vector3 pos = Unprojection(vector2((float)x , (float)y));

	  for (int i = 0 ; i < featureList.size() ; i++)
	  {
		  int idx = featureList[i];
		  vector3 pt(mesh->vertices[3 * idx + 0] , mesh->vertices[3 * idx + 1] , mesh->vertices[3 * idx + 2]);
		  dis = (pos - pt).length();

		  if (minDis > dis)
		  {
			  minDis = dis;
			  minIdx = featureList[i];
		  }
		  //cout << "x: " << pt.x << endl;
	  }

	  selectedFeature = minIdx;
	  //TODO: Deform Features
	  PointDis();
	  RBF();
	  VectorToMatrix();
	  CalculateWeight();

	  //Calculate wv value
	  for (int i = 0; i < wValue.size(); i++)
	  {
		  vector3 wv = wv + wValue[i] * dis;
		  wvValue.push_back(wv);
		  //cout << "WV Value: " << wvValue[i] << endl;
	  }
	  //Calculate the changes of dValue
	  for (int i = 0; i < rbfList.size(); i++)
	  {
		  //wvValue[i] = {0,0,0};
		  for (int j = 0; j < rbfList.size(); j++)
		  {
			  vector3 dC = dC + (wvValue[i] * rbfMat[i][j]);
			  dChange.push_back(dC);
		  }
		  //cout << "dChange Value: " << dChange[i] << endl;
	  }
	  /*cout << "Final D: " << endl;
	  for (int i = 0; i < d.size(); i++)
	  {
		  vector3 dD;
		  dD.x = d[i].x * dChange[i];
		  dD.y = d[i].y * dChange[i];
		  dD.z = d[i].z * dChange[i];
		  finalD.push_back(dD);
		  cout << finalD[i].x << " " << finalD[i].y << " " << finalD[i].z << endl;
	  }*/

  }

  if (button == GLUT_RIGHT_BUTTON && state == GLUT_UP)
  {
	  selectedFeature = -1;
	  pointDisList.clear();
	  rbfList.clear();
	  wValue.clear();
	  wvValue.clear();
	  dChange.clear();
  }

  last_x = x;
  last_y = y;
  
}

void motion(int x, int y)
{
  tbMotion(x, y);

  if (selectedFeature != -1)
  {
	  matrix44 m;
	  vector4 vec;
	  //vector4 vec = vector4((float)(x - last_x) / 100.0f , (float)(y - last_y) / 100.0f , 0.0 , 1.0);
	  for (int i = 0; i < featureList.size(); i++)
	  {
		  int idx = featureList[i];
		  vec = vector4(dChange[i].x / 10000, dChange[i].y / 10000, 0.0, 1.0);
		  gettbMatrix((float *)&m);
		  vec = m * vec;
	  }

	  for (int i = 0; i < featureList.size(); i++)
	  {
		  int idx = featureList[i];

		  mesh->vertices[3 * idx + 0] += vec.x;
		  mesh->vertices[3 * idx + 1] -= vec.y;
		  mesh->vertices[3 * idx + 2] += vec.z;
	  }
	  /*mesh->vertices[3 * selectedFeature + 0] += vec.x;
	  mesh->vertices[3 * selectedFeature + 1] -= vec.y;
	  mesh->vertices[3 * selectedFeature + 2] += vec.z;*/
  }

  last_x = x;
  last_y = y;
}

void timf(int value)
{
  glutPostRedisplay();
  glutTimerFunc(1, timf, 0);
}

int main(int argc, char *argv[])
{
  WindWidth = 400;
  WindHeight = 400;
	
  GLfloat light_ambient[] = {0.0, 0.0, 0.0, 1.0};
  GLfloat light_diffuse[] = {0.8, 0.8, 0.8, 1.0};
  GLfloat light_specular[] = {1.0, 1.0, 1.0, 1.0};
  GLfloat light_position[] = {0.0, 0.0, 1.0, 0.0};

  glutInit(&argc, argv);
  glutInitWindowSize(WindWidth, WindHeight);
  glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
  glutCreateWindow("Trackball Example");

  glutReshapeFunc(Reshape);
  glutDisplayFunc(Display);
  glutMouseFunc(mouse);
  glutMotionFunc(motion);
  glClearColor(0, 0, 0, 0);

  glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
  glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
  glLightfv(GL_LIGHT0, GL_POSITION, light_position);

  glEnable(GL_LIGHT0);
  glDepthFunc(GL_LESS);

  glEnable(GL_DEPTH_TEST);
  glEnable(GL_LIGHTING);
  glEnable(GL_COLOR_MATERIAL);
  tbInit(GLUT_LEFT_BUTTON);
  tbAnimate(GL_TRUE);

  glutTimerFunc(40, timf, 0); // Set up timer for 40ms, about 25 fps

  // load 3D model
  mesh = glmReadOBJ("../data/head.obj");
  
  glmUnitize(mesh);
  glmFacetNormals(mesh);
  glmVertexNormals(mesh , 90.0);

  glutMainLoop();

  return 0;

}


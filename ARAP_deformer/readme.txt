使用方法

1.切換模式
s: selection mode(進入程式時預設)
d: deformation mode

2. selection mode
滑鼠左鍵: 旋轉模型
滑鼠右鍵: 製造handle; 
          "按下->拖拉->放開"這一連串動作所定義的box中包含的所有3D點會成為一個handle

3. deformation mode
滑鼠左鍵: 旋轉模型
滑鼠右鍵: 形變; 被變形的handle是最靠近按下右鍵處的那一個

Instructions

Switch mode
s: selection mode (preset when entering the program)
D: deformation mode

2. selection mode
Left mouse button: Rotate model
Right mouse button: make handle;
           "Press -> Drag -> Release" All the 3D points contained in the box defined by this series of actions will become a handle.

Deformation mode
Left mouse button: Rotate model
Right mouse button: deformation; the deformed handle is the one closest to the right button
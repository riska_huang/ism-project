#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <vector>
#include <numeric>
#include <iostream>
#include <limits>
#include <Windows.h>
#include <gl/GL.h>
#include <glut.h>

#include "glm.h"
#include "mtxlib.h"
#include "trackball.h"

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <fenv.h>
#include <unordered_map>
#pragma STDC FENV_ACCESS ON

using namespace std;
using namespace Eigen;

/*
 * Interactive Shape Manipulation Homework 3
 * As-Rigid-As Possible Surface Modeling
 * Name: Riska Wilian Triany Putri - ���мz
 * Student ID: 0656643
 * Note: need to wait for quite a long time for the result to come out.
 * I do my best but I think there's something wrong with my initial guess of p'
 * I think it need more looping but, it need large amount of time
 * Thank you :D
 */

// ----------------------------------------------------------------------------------------------------
// global variables

_GLMmodel *mesh;

int WindWidth, WindHeight;
int last_x , last_y;
int select_x, select_y;

typedef enum { SELECT_MODE, DEFORM_MODE } ControlMode;
ControlMode current_mode = SELECT_MODE;

vector<float*> colors;
vector<vector<int> > handles;
int selected_handle_id = -1;
bool deform_mesh_flag = false;

//Added Variable
vector<int>freeVert;
vector<double> energyList;
map<string, double> energyMap;
vector<vector3>allVertPost;
vector<vector<int>> faceIndices;
vector<unordered_map<int, int>> neighborList;
vector<MatrixXd> rotationList;
SparseMatrix<double> weight;
SparseMatrix<double>LMatrix;
MatrixXd allVPos;
MatrixXd updatedVPos;
MatrixXi fIndices;
SimplicialCholesky<SparseMatrix<double>> cholMatrix;
// Iteration Counter
int iteration;


// ----------------------------------------------------------------------------------------------------
// render related functions

void Reshape(int width, int height)
{
	int base = min(width , height);

	tbReshape(width, height);
	glViewport(0 , 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0,(GLdouble)width / (GLdouble)height , 1.0, 128.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0, 0.0, -3.5);

	WindWidth = width;
	WindHeight = height;
}

void Display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glPushMatrix();
	tbMatrix();

	// render solid model
	glEnable(GL_LIGHTING);
	glColor3f(1.0 , 1.0 , 1.0f);
	glPolygonMode(GL_FRONT_AND_BACK , GL_FILL);
	glmDraw(mesh , GLM_SMOOTH);

	// render wire model
	glPolygonOffset(1.0 , 1.0);
	glEnable(GL_POLYGON_OFFSET_FILL);
	glLineWidth(1.0f);
	glColor3f(0.6 , 0.0 , 0.8);
	glPolygonMode(GL_FRONT_AND_BACK , GL_LINE);
	glmDraw(mesh , GLM_SMOOTH);

	// render handle points
	glPointSize(10.0);
	glEnable(GL_POINT_SMOOTH);
	glDisable(GL_LIGHTING);
	glBegin(GL_POINTS);
	for(int handleIter=0; handleIter<handles.size(); handleIter++)
	{
		glColor3fv(colors[ handleIter%colors.size() ]);
		for(int vertIter=0; vertIter<handles[handleIter].size(); vertIter++)
		{
			int idx = handles[handleIter][vertIter];
			glVertex3fv((float *)&mesh->vertices[3 * idx]);
		}
	}
	glEnd();

	glPopMatrix();

	glFlush();  
	glutSwapBuffers();
}

//TODO:: Insert all function here

#pragma region Initialization
//Generate the data that will be needed.
void GenerateData()
{
	//clear the vector list
	allVertPost.clear();
	faceIndices.clear();
	freeVert.clear();

	//Generate face's indices data for all vertices
	for(int f = 0; f < mesh->numvertices; f++)
	{
		vector<int>triangleVector;
		for (int i = 0; i < mesh->numtriangles; i++)
		{
			if (f == mesh->triangles[i].vindices[0])
			{
				for(int j = 0; j < 3; j++)
				{
					triangleVector.push_back(mesh->triangles[i].vindices[j]);
				}
			}
		}
		faceIndices.push_back(triangleVector);
	}

	//Generate all vertices position matrix data
	//Generate not selected vertices indices data
	for(int k = 0; k < mesh->numvertices; k++)
	{
		//Generate all vertices position matrix data
		vector3 pt(mesh->vertices[3 * k + 0], mesh->vertices[3 * k + 1], mesh->vertices[3 * k + 2]);
		allVertPost.push_back(pt);
		for (int handleIter = 0; handleIter < handles.size(); handleIter++)
		{
			if (find(handles[handleIter].begin(), handles[handleIter].end(), k) != handles[handleIter].end()) {
				//contains
			}
			else {
				//not contain
				freeVert.push_back(k);
			}
		}
	}
}

//Convert the vector list into eigen vector for easier calculation
void ConvertVectortoEigen()
{
	fIndices.resize(faceIndices.size(), 50);
	allVPos.resize(allVertPost.size(), 3);
	for(int j = 0; j < faceIndices.size(); j++)
	{
		for(int l = 0; l < faceIndices[j].size(); l++)
		{
			fIndices(j, l) = faceIndices[j][l];
		}
	}
	for(int k = 0; k < allVertPost.size(); k++)
	{
		allVPos(k, 0) = allVertPost[k].x;
		allVPos(k, 1) = allVertPost[k].y;
		allVPos(k, 2) = allVertPost[k].z;
	}
	faceIndices.clear();
	allVertPost.clear();
}

//Compute cotangent of the triangle to compute the weight
Vector3d ComputeCotangent(int fId) 
{
	Vector3d cotangent(0.0, 0.0, 0.0);
	Vector3d A = allVPos.row(fIndices(fId, 0));
	Vector3d B = allVPos.row(fIndices(fId, 1));
	Vector3d C = allVPos.row(fIndices(fId, 2));
	//Compute the triangle edge length
	double a2 = (B - C).squaredNorm();
	double b2 = (C - A).squaredNorm();
	double c2 = (A - B).squaredNorm();

	//Compute the area of the triangle
	double area = (B - A).cross(C - A).norm();

	//Compute Cotangent
	cotangent(0) = (b2 + c2 - a2) / (4 * area);
	cotangent(1) = (c2 + a2 - b2) / (4 * area);
	cotangent(2) = (a2 + b2 - c2) / (4 * area);
	return cotangent;
}

//Compute the weight for each vertices
void ComputeWeight()
{
	ConvertVectortoEigen();
	//Compute Weight
	weight.resize(allVPos.rows(), allVPos.rows());
	int index_map[3][2] = { { 1, 2 },{ 2, 0 },{ 0, 1 } };
	for (int i = 0; i < fIndices.rows(); i++)
	{
		Vector3d cotangent = ComputeCotangent(i);

		for(int j = 0; j < 3; j++)
		{
			int first = fIndices(i, index_map[j][0]);
			int second = fIndices(i, index_map[j][1]);
			weight.coeffRef(first, second) += (cotangent(j) / 2);
			weight.coeffRef(second, first) += (cotangent(j) / 2);
			weight.coeffRef(first, first) -= (cotangent(j) / 2);
			weight.coeffRef(second, second) -= (cotangent(j) / 2);
		}
	}
}

//Detect and generate the neighbor vertices
void ComputeNeighbor()
{
	int index_map[3][2] = { { 1, 2 },{ 2, 0 },{ 0, 1 } };
	neighborList.resize(allVPos.rows());
	for (int i = 0; i < fIndices.rows(); i++) {
		for (int j = 0; j < 3; j++) {
			int first = fIndices(i, index_map[j][0]);
			int second = fIndices(i, index_map[j][1]);
			neighborList[first][second] = second;
			neighborList[second][first] = first;
		}
	}
}

//Generate L matrix in Lp' = b
void GenerateLMatrix()
{
	LMatrix.setZero();
	SparseMatrix<double> LTLMatrix;
	LMatrix.resize(allVPos.rows(), allVPos.rows());
	for(int i = 0; i < freeVert.size(); i++)
	{
		int idx = freeVert[i];
		for(auto& neighbor : neighborList[idx])
		{
			int neighborIdx = neighbor.first; //neighbor Index
			double weightN = weight.coeff(idx, neighborIdx);
			LMatrix.coeffRef(i, i) += weightN;
			if (find(freeVert.begin(), freeVert.end(), neighborIdx) != freeVert.end()) {
				//contains
				LMatrix.coeffRef(i, neighborIdx) -= weightN;
			}
		}
	}
	LMatrix.makeCompressed();

	//Use Cholesky Factorization
	LTLMatrix = LMatrix.transpose() * LMatrix;
	cholMatrix.compute(LTLMatrix);
}

//Initialize
void Initialization()
{
	GenerateData();
	ComputeWeight();
	ComputeNeighbor();
	GenerateLMatrix();
}
#pragma endregion

#pragma region Pre-Processing

//Calculate the weight * p'
//It gives weird result, so I don't use the entire function
void CalculateWP()
{
	SparseMatrix<double> freeW;
	freeW.resize(allVPos.rows(), allVPos.rows());
	SparseMatrix<double> handlesW;
	handlesW.resize(allVPos.rows(), allVPos.rows());
	for(int i = 0; i < freeVert.size(); i++)
	{
		int fdx = freeVert[i];
		for(auto& neighbor :  neighborList[i])
		{
			int ndx = neighbor.first;
			double weightN = weight.coeff(fdx, ndx);
			freeW.coeffRef(fdx, ndx) = (allVPos(fdx, 0) * weightN)
				+ (allVPos(fdx, 1) * weightN) + (allVPos(fdx, 2) * weightN);
			//freeW.row(i) += allVPos.row(fdx) * weightN;
		}
	}
	for(int i = 0; i < handles[selected_handle_id].size(); i++)
	{
		int idx = handles[selected_handle_id][i];
		for (auto& neighbor : neighborList[i])
		{
			int ndx = neighbor.first;
			double weightN = weight.coeff(idx, ndx);
			handlesW.coeffRef(idx, ndx) = (allVPos(idx, 0) * weightN)
				+ (allVPos(idx, 1) * weightN) + (allVPos(idx, 2) * weightN);
		}
	}
	SparseMatrix<double> fTf;
	fTf.resize(freeVert.size(), freeVert.size());
	fTf = freeW.transpose() * freeW;
	fTf.makeCompressed();
	SimplicialCholesky<SparseMatrix<double>> chols;
	chols.compute(fTf);

	for(int c = 0; c < 3; c++)
	{
		VectorXd handlesw = weight * allVPos.col(c) - handlesW * updatedVPos.col(c);
		VectorXd fTh = freeW.transpose() * handlesw;
		VectorXd x = chols.solve(fTh);

		for (int i = 0; i < freeVert.size(); i++) {
			updatedVPos(freeVert[i], c) = x(i);
		}
	}
	for (int i = 0; i < handles[selected_handle_id].size(); i++) {
		Vector3d a = allVPos.row(i);
		updatedVPos.row(handles[selected_handle_id][i]) = a;
	}

}

//Calculate the initial rotation value
void CalculateInitRotation()
{
	rotationList.clear();
	vector<Matrix3d> edgeProduct(allVPos.rows(),Matrix3d::Zero());
	for (int i = 0; i < allVPos.rows(); ++i) {
		//Matrix3d edgeProduct = Matrix3d::Zero();
		for (auto& neighbor : neighborList[i]) {
			int j = neighbor.first;
			double weightN = weight.coeff(i, j);
			Vector3d edge = allVPos.row(i) - allVPos.row(j);
			Vector3d edgeUpdate = updatedVPos.row(i) - updatedVPos.row(j);
			edgeProduct[i] += weightN * edge * edgeUpdate.transpose();
		}
		//Do SVD to get the rotation
		JacobiSVD<Matrix3d> svd(edgeProduct[i], ComputeFullU | ComputeFullV);
		MatrixXd sValue = svd.singularValues();
		rotationList.push_back(sValue); //if the result is weird, pushback the tranpose
	}
	edgeProduct.clear();
}

//Calculate the initial guess of p' and initial rotation
void PreProcessing()
{
	//CalculateWP();
	CalculateInitRotation();
}
#pragma endregion 

#pragma region Processing
//Get Energy
void ComputeEnergy()
{
	double total = 0.0;
	energyList.resize(allVPos.rows());
	for(int i = 0; i < allVPos.rows(); i++)
	{
		for(auto& neighbor : neighborList[i])
		{
			int j = neighbor.first;
			double weightN = weight.coeff(i, j);
			double edgeEnergy = 0.0;
			Vector3d calcVector =
				(updatedVPos.row(i) - updatedVPos.row(j)).transpose() -
				rotationList[i] * (allVPos.row(i) - allVPos.row(j)).transpose();
			edgeEnergy = weightN * calcVector.squaredNorm();
			total += edgeEnergy;
		}
		energyList[i] = total;
	}
}

//Computed the updated rotation with the new p' value
void ComputeUpdatedRotation()
{
	//Compute the updated Rotation
	for (int i = 0; i < allVPos.rows(); ++i) {
		Matrix3d edgeProduct = Matrix3d::Zero();
		for (auto& neighbor : neighborList[i]) {
			int j = neighbor.first;
			double weightN = weight.coeff(i, j);
			Vector3d edge = allVPos.row(i) - allVPos.row(j);
			Vector3d edgeUpdate = updatedVPos.row(i) - updatedVPos.row(j);
			edgeProduct += weightN * edge * edgeUpdate.transpose();
		}
		//Do SVD to get the rotation
		JacobiSVD<Matrix3d> svd(edgeProduct, ComputeFullU | ComputeFullV);
		MatrixXd sValue = svd.singularValues();
		rotationList[i] = sValue; //if the result is weird, pushback the tranpose
	}
}

//Compute the updated vertices position (p') value
MatrixXd ComputeTheRight()
{
	//Compute the Right
	MatrixXd rhs = MatrixXd::Zero(allVPos.rows(), 3);
	for (int i = 0; i < freeVert.size(); ++i) {
		int idx = freeVert[i];
		for (auto& neighbor : neighborList[idx]) {
			int ndx = neighbor.first;
			if (ndx != NULL)
			{
				double weightN = weight.coeff(idx, ndx);
				Vector3d rotationA = rotationList[idx] + rotationList[ndx];
				Vector3d pos = (allVPos.row(idx) - allVPos.row(ndx)).transpose();
				/*Vector3d calcVector = weightN / 2.0
					* (rotationList[idx] + rotationList[ndx])
					* (allVPos.row(idx) - allVPos.row(ndx)).transpose();*/
				Vector3d calcVector = (weightN / 2) * rotationA;
				calcVector(0, 0) = calcVector(0, 0) * pos(0, 0);
				calcVector(0, 1) = calcVector(0, 1) * pos(0, 1);
				calcVector(0, 2) = calcVector(0, 2) * pos(0, 2);
				Vector3d rhsV = rhs.row(i);
				Vector3d result = rhsV + calcVector;
				rhs.row(i) = result;
				if (find(freeVert.begin(), freeVert.end(), ndx) != freeVert.end()) {
					//contains
				}
				else
				{
					//does not contain
					Vector3d w = weightN * updatedVPos.row(ndx);
					rhs.row(i) += w;
				}
			}
		}
	}
	return rhs;
}

void SolveOneIteration()
{
	ComputeUpdatedRotation();
	MatrixXd rhs = ComputeTheRight();

	//Use Cholesky Solver to solve for the updated p'
	//Compute ATB
	MatrixXd LTRMatrix = LMatrix.transpose() * rhs;
	MatrixXd result = cholMatrix.solve(LTRMatrix);
	updatedVPos = result;
}

//Iterate the function until it give perfect deformation
void Processing()
{
	if(iteration == 0)
	{
		ComputeEnergy();
		while (iteration < 2) {  //1000 max 
			cout << "Iteration: " << iteration << endl;
			SolveOneIteration();
			ComputeEnergy();
			iteration++;
		}
	}
}
#pragma endregion 
// ----------------------------------------------------------------------------------------------------
// mouse related functions

vector3 Unprojection(vector2 _2Dpos)
{
	float Depth;
	int viewport[4];
	double ModelViewMatrix[16];    // Model_view matrix
	double ProjectionMatrix[16];   // Projection matrix

	glPushMatrix();
	tbMatrix();

	glGetIntegerv(GL_VIEWPORT, viewport);
	glGetDoublev(GL_MODELVIEW_MATRIX, ModelViewMatrix);
	glGetDoublev(GL_PROJECTION_MATRIX, ProjectionMatrix);

	glPopMatrix();

	glReadPixels((int)_2Dpos.x , viewport[3] - (int)_2Dpos.y , 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &Depth);

	double X = _2Dpos.x;
	double Y = _2Dpos.y;
	double wpos[3] = {0.0 , 0.0 , 0.0};

	gluUnProject(X , ((double)viewport[3] - Y) , (double)Depth , ModelViewMatrix , ProjectionMatrix , viewport, &wpos[0] , &wpos[1] , &wpos[2]);

	return vector3(wpos[0] , wpos[1] , wpos[2]);
}

vector2 projection_helper(vector3 _3Dpos)
{
	int viewport[4];
	double ModelViewMatrix[16];    // Model_view matrix
	double ProjectionMatrix[16];   // Projection matrix

	glPushMatrix();
	tbMatrix();

	glGetIntegerv(GL_VIEWPORT, viewport);
	glGetDoublev(GL_MODELVIEW_MATRIX, ModelViewMatrix);
	glGetDoublev(GL_PROJECTION_MATRIX, ProjectionMatrix);

	glPopMatrix();

	double wpos[3] = {0.0 , 0.0 , 0.0};
	gluProject(_3Dpos.x, _3Dpos.y, _3Dpos.z, ModelViewMatrix, ProjectionMatrix, viewport, &wpos[0] , &wpos[1] , &wpos[2]);

	return vector2(wpos[0], (double)viewport[3]-wpos[1]);
}

void mouse(int button, int state, int x, int y)
{
	tbMouse(button, state, x, y);

	if( current_mode==SELECT_MODE && button==GLUT_RIGHT_BUTTON )
	{
		if(state==GLUT_DOWN)
		{
			select_x = x;
			select_y = y;
		}
		else
		{
			iteration = 0;
			vector<int> this_handle;

			// project all mesh vertices to current viewport
			for(int vertIter=0; vertIter<mesh->numvertices; vertIter++)
			{
				vector3 pt(mesh->vertices[3 * vertIter + 0] , mesh->vertices[3 * vertIter + 1] , mesh->vertices[3 * vertIter + 2]);
				vector2 pos = projection_helper(pt);

				// if the projection is inside the box specified by mouse click&drag, add it to current handle
				if(pos.x>=select_x && pos.y>=select_y && pos.x<=x && pos.y<=y)
				{
					this_handle.push_back(vertIter);
				}
			}
			handles.push_back(this_handle);
		}
	}
	// select handle
	else if( current_mode==DEFORM_MODE && button==GLUT_RIGHT_BUTTON && state==GLUT_DOWN )
	{
		// project all handle vertices to current viewport
		// see which is closest to selection point
		double min_dist = 999999;
		int handle_id = -1;
		for(int handleIter=0; handleIter<handles.size(); handleIter++)
		{
			for(int vertIter=0; vertIter<handles[handleIter].size(); vertIter++)
			{
				int idx = handles[handleIter][vertIter];
				vector3 pt(mesh->vertices[3 * idx + 0] , mesh->vertices[3 * idx + 1] , mesh->vertices[3 * idx + 2]);
				vector2 pos = projection_helper(pt);

				double this_dist = sqrt((double)(pos.x-x)*(pos.x-x) + (double)(pos.y-y)*(pos.y-y));
				if(this_dist<min_dist)
				{
					min_dist = this_dist;
					handle_id = handleIter;
				}
			}
		}

		selected_handle_id = handle_id;
		deform_mesh_flag = true;
	}

	if(button == GLUT_RIGHT_BUTTON && state == GLUT_UP)
		deform_mesh_flag = false;

	last_x = x;
	last_y = y;
}



void motion(int x, int y)
{
	tbMotion(x, y);

	// if in deform mode and a handle is selected, deform the mesh
	if( current_mode==DEFORM_MODE && deform_mesh_flag==true )
	{
		matrix44 m;
		vector4 vec = vector4((float)(x - last_x) / 1000.0f , (float)(y - last_y) / 1000.0f , 0.0 , 1.0);

		gettbMatrix((float *)&m);
		vec = m * vec;
		updatedVPos.setZero();
		updatedVPos.resize(mesh->numvertices, 3);
		cout << "Start Deforming the Mesh..." << endl;
		//Initialize
		Initialization();
		//Guess the first p' (Updated vertices Position) value
		for(int vertIter=0; vertIter<handles[selected_handle_id].size(); vertIter++)
		{
			int idx = handles[selected_handle_id][vertIter];
			for (auto& neighbor : neighborList[idx]) 
			{
				int ndx = neighbor.first;
				//vector3 pt(mesh->vertices[3*idx+0]+vec.x, mesh->vertices[3*idx+1]+vec.y, mesh->vertices[3*idx+2]+vec.z);
				double x = vec.x;
				double y = vec.y;
				double z = vec.z;
				updatedVPos(idx, 0) = x;
				updatedVPos(idx, 1) = y;
				updatedVPos(idx, 2) = z;
			}
			
		}
		// deform handle points
		PreProcessing();
		Processing();
		//Write the result back into the mesh
		for(int i = 0; i < mesh->numvertices; i++)
		{
			mesh->vertices[3 * i + 0] = allVPos(i, 0) + updatedVPos(i, 0);
			mesh->vertices[3 * i + 1] = allVPos(i, 1) + updatedVPos(i, 1);
			mesh->vertices[3 * i + 2] = allVPos(i, 2) + updatedVPos(i, 2);
		}
		cout << "Deformed" << endl;
		cout << "Wait until 2x 'Deformed' text come out, then done" << endl;
	}

	last_x = x;
	last_y = y;
}

// ----------------------------------------------------------------------------------------------------
// keyboard related functions

void keyboard(unsigned char key, int x, int y )
{
	switch(key)
	{
	case 'd':
		current_mode = DEFORM_MODE;
		break;
	default:
	case 's':
		current_mode = SELECT_MODE;
		break;
	}
}

// ----------------------------------------------------------------------------------------------------
// main function

void timf(int value)
{
	glutPostRedisplay();
	glutTimerFunc(1, timf, 0);
}

int main(int argc, char *argv[])
{
	// compute SVD decomposition of a matrix m
	// SVD: m = U * S * V^T
	//Eigen::MatrixXf m = Eigen::MatrixXf::Random(3,2);
	//cout << "Here is the matrix m:" << endl << m << endl;
	//Eigen::JacobiSVD<Eigen::MatrixXf> svd(m, Eigen::ComputeFullU | Eigen::ComputeFullV);
	//const Eigen::Matrix3f U = svd.matrixU();
	//// note that this is actually V^T!!
	//const Eigen::Matrix3f V = svd.matrixV();
	//const Eigen::VectorXf S = svd.singularValues();

	WindWidth = 800;
	WindHeight = 800;

	GLfloat light_ambient[] = {0.0, 0.0, 0.0, 1.0};
	GLfloat light_diffuse[] = {0.8, 0.8, 0.8, 1.0};
	GLfloat light_specular[] = {1.0, 1.0, 1.0, 1.0};
	GLfloat light_position[] = {0.0, 0.0, 1.0, 0.0};

	// color list for rendering handles
	float red[] = {1.0, 0.0, 0.0};
	colors.push_back(red);
	float yellow[] = {1.0, 1.0, 0.0};
	colors.push_back(yellow);
	float blue[] = {0.0, 1.0, 1.0};
	colors.push_back(blue);
	float green[] = {0.0, 1.0, 0.0};
	colors.push_back(green);

	glutInit(&argc, argv);
	glutInitWindowSize(WindWidth, WindHeight);
	glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
	glutCreateWindow("ARAP");

	glutReshapeFunc(Reshape);
	glutDisplayFunc(Display);
	glutMouseFunc(mouse);
	glutMotionFunc(motion);
	glutKeyboardFunc(keyboard);
	glClearColor(0, 0, 0, 0);

	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	glEnable(GL_LIGHT0);
	glDepthFunc(GL_LESS);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);
	tbInit(GLUT_LEFT_BUTTON);
	tbAnimate(GL_TRUE);

	glutTimerFunc(40, timf, 0); // Set up timer for 40ms, about 25 fps

	// load 3D model
	mesh = glmReadOBJ("../data/man.obj");

	glmUnitize(mesh);
	glmFacetNormals(mesh);
	glmVertexNormals(mesh , 90.0);

	glutMainLoop();

	return 0;

}
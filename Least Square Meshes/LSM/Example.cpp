#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <vector>
#include <numeric>
#include <iostream>
#include <Windows.h>
#include <gl/GL.h>
#include <glut.h>
#include <Eigen/Sparse>
#include <Eigen/Core>

#include "glm.h"
#include "mtxlib.h"
#include "trackball.h"
#include <string>
#include <ctime>

using namespace std;
using namespace Eigen;

/*
 * Interactive Shape Manipulation Homework 2
 * Least Mean Square
 * Name: Riska Wilian Triany Putri - ���мz
 * Student ID: 0656643
 * Last Edited: 2018-10-28
 */

_GLMmodel *mesh;
int WindWidth, WindHeight;

int last_x , last_y;
int selectedFeature = -1;
vector<int> featureList;
vector<vector<int>>nextVertices;

void Reshape(int width, int height)
{
  int base = min(width , height);

  tbReshape(width, height);
  glViewport(0 , 0, width, height);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(45.0,(GLdouble)width / (GLdouble)height , 1.0, 128.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glTranslatef(0.0, 0.0, -3.5);

  WindWidth = width;
  WindHeight = height;
}

void Display(void)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glPushMatrix();
  tbMatrix();
  
  // render solid model
  glEnable(GL_LIGHTING);
  glColor3f(1.0 , 1.0 , 1.0f);
  glPolygonMode(GL_FRONT_AND_BACK , GL_FILL);
  glmDraw(mesh , GLM_SMOOTH);

  // render wire model
  glPolygonOffset(1.0 , 1.0);
  glEnable(GL_POLYGON_OFFSET_FILL);
  glLineWidth(1.0f);
  glColor3f(0.6 , 0.0 , 0.8);
  glPolygonMode(GL_FRONT_AND_BACK , GL_LINE);
  glmDraw(mesh , GLM_SMOOTH);

  // render features
  glPointSize(10.0);
  glColor3f(1.0 , 0.0 , 0.0);
  glDisable(GL_LIGHTING);
  glBegin(GL_POINTS);
	for (int i = 0 ; i < featureList.size() ; i++)
	{
		int idx = featureList[i];

		glVertex3fv((float *)&mesh->vertices[3 * idx]);
	}
  glEnd();
  
  glPopMatrix();

  glFlush();  
  glutSwapBuffers();
}

vector3 Unprojection(vector2 _2Dpos)
{
	float Depth;
	int viewport[4];
	double ModelViewMatrix[16];				//Model_view matrix
	double ProjectionMatrix[16];			//Projection matrix

	glPushMatrix();
	tbMatrix();

	glGetIntegerv(GL_VIEWPORT, viewport);
	glGetDoublev(GL_MODELVIEW_MATRIX, ModelViewMatrix);
	glGetDoublev(GL_PROJECTION_MATRIX, ProjectionMatrix);

	glPopMatrix();

	glReadPixels((int)_2Dpos.x , viewport[3] - (int)_2Dpos.y , 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &Depth);

	double X = _2Dpos.x;
	double Y = _2Dpos.y;
	double wpos[3] = {0.0 , 0.0 , 0.0};

	gluUnProject(X , ((double)viewport[3] - Y) , (double)Depth , ModelViewMatrix , ProjectionMatrix , viewport, &wpos[0] , &wpos[1] , &wpos[2]);

	return vector3(wpos[0] , wpos[1] , wpos[2]);
}

void mouse(int button, int state, int x, int y)
{
  tbMouse(button, state, x, y);

  // Randomly select 1000 vertices
  if (button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN)
  {
	  if(featureList.empty())
	  {
		  for (int i = 0; i < 986; i++)
		  {
			  int randPoint = rand() % mesh->numvertices + 1;
			  featureList.push_back(randPoint);
		  }
	  }
	  else if(featureList.empty() == false)
	  {
		  featureList.clear();
	  }
  }

  // manipulate feature
  if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
  {
	  nextVertices.clear();
	  //TODO::Timer --> to measure how long the process will take place
	  clock_t startClock;
	  double durationClock;
	  startClock = clock();

	  //TODO:: Perform Least Square Meshes Here
	  // - Find number of neighbor for each control point
	  // - find whether the neighbor is selected or not
	  // - find set of vertices that are selected as control point
	  // - solve the equation using Eigen
	  vector<int>nextVerticesList;
	  vector<int>neighborNumList;
	  vector<int>neighborList;
	  vector<vector<int>>relatedCPoint;
	  vector<vector<int>>relatedNPoint;
	  vector<vector<double>>dList;
	  vector<vector3>cList;
	  vector<vector3>vList;

	  //- Find the neighbor vertex indices
	  //- Arrange neighbor into 1 list A per 1 control point, erase the same index
	  //- Find the size of the list A --> store it in the list with the same size as featureList = d List G 
	  // ^ maybe wrong, need to find the size of the neighbor point instead
	  int size = 0;
	  for (int k = 0; k < featureList.size(); k++)
	  {
		  int idx = featureList[k];
		  for (int i = 0; i < mesh->numtriangles; i++) 
		  {
			  for (int j = 0; j < 3; j++) 
			  {
					if (idx == mesh->triangles[i].vindices[j]) 
					{
						nextVertices.push_back(vector<int>());
						int nextVrtSize = nextVertices.size();
						nextVertices[nextVrtSize - 1].push_back(mesh->triangles[i].vindices[0]);
						nextVertices[nextVrtSize - 1].push_back(mesh->triangles[i].vindices[1]);
						nextVertices[nextVrtSize - 1].push_back(mesh->triangles[i].vindices[2]);
						//nextVertices[n].push_back(mesh->triangles[i].vindices[3]);
						//cout << idx << ": " << nextVertices[nextVrtSize - 1][0] << ", " << nextVertices[nextVrtSize - 1][1] << ", " << nextVertices[nextVrtSize - 1][2] << endl;
				  }
			  }
		  }
		  for(int l = size; l < nextVertices.size(); l++)
		  {
			  for(int m = 0; m < nextVertices[l].size(); m++)
			  {
				  bool same = false;
				  int checkInt = nextVertices[l][m];
				  if (nextVerticesList.empty())
				  {
					  nextVerticesList.push_back(checkInt);
				  }
				  else
				  {
					  for (int k = 0; k < nextVerticesList.size(); k++)
					  {
						  if (checkInt == nextVerticesList[k])
						  {
							  same = true;
						  }
					  }
					  if (same == false)
					  {
						  nextVerticesList.push_back(checkInt);
					  }
				  }
			  }
		  }
	  }

	  //- Separate control point and neighbor point, store neighbor point as List F
	  neighborList = nextVerticesList; //duplicate the list
	  for (auto j = neighborList.begin(); j != neighborList.end(); ++j)
	  {
		  for (int i = 0; i < featureList.size(); i++)
		  {
			  int idx = featureList[i];
			  if (*j == idx)
			  {
				  neighborList.erase(j);
				  j--;
			  }
		  }
	  }
	  nextVerticesList.clear();

	  //TODO:: Find 1/4 (C1+C2)
	  //- Loop through the neighbor of each neighbor point. 
	  //- Check whether the neighbor is a control point
	  //- If it is a control point, store it in the multidimensionalList
	  //- Find the neighbor size of each neighbor Point
	  for(int i = 0; i < neighborList.size(); i++)
	  {
		  int ndx = neighborList[i];
		  relatedCPoint.push_back(vector<int>());
		  relatedNPoint.push_back(vector<int>());
		  for(int j = 0; j < featureList.size(); j++)
		  {
			  int idx = featureList[j];
			  for(int k = 0; k < mesh->numtriangles; k++)
			  {
				  for(int l = 0; l < 3; l++)
				  {
					  if(ndx == mesh->triangles[k].vindices[l])
					  {
						  vector<int>tempNeighbor;
						  int n1 = mesh->triangles[k].vindices[0];
						  int n2 = mesh->triangles[k].vindices[1];
						  int n3 = mesh->triangles[k].vindices[2];
						  tempNeighbor.push_back(n1);
						  tempNeighbor.push_back(n2);
						  tempNeighbor.push_back(n3);
						  if(find(tempNeighbor.begin(), tempNeighbor.end(), idx) != tempNeighbor.end())
						  {
							  if(relatedCPoint[i].empty())
							  {
								  relatedCPoint[i].push_back(idx);
							  }
							  else
							  {
								  if (find(relatedCPoint[i].begin(), relatedCPoint[i].end(), idx) != relatedCPoint[i].end()){}
								  else { relatedCPoint[i].push_back(idx); }
							  }
						  }
						  if(relatedNPoint[i].empty())
						  {
							  for(int m = 0; m < tempNeighbor.size(); m++)
							  {
								relatedNPoint[i].push_back(tempNeighbor[m]);
							  }
						  }
						  else
						  {
							  for (int m = 0; m < tempNeighbor.size(); m++)
							  {
								  if (find(relatedNPoint[i].begin(), relatedNPoint[i].end(), tempNeighbor[m]) != relatedNPoint[i].end()) {}
								  else 
								  {
									relatedNPoint[i].push_back(tempNeighbor[m]); 
								  }
							  }
						  }
					  }
				  }
			  }
		  }
		  neighborNumList.push_back(relatedNPoint[i].size());
	  }

	  //--------------------------------------------------------------

	  //TODO::Create Multidimensional List to store 1/d information for each neighbor
	  //Loop through list of neighbor point, check whether it is the same as relatedNpoint
	  //If same = store it as multidimensional array as 1, if it's neighbor, store it as 1/d, if none, store it as 0
	  
	  for(int i = 0; i < neighborList.size(); i++)
	  {
		  int ndx = neighborList[i];
		  dList.push_back(vector<double>());
		  for(int k = 0; k < neighborList.size(); k++)
		  {
			  int dNum = neighborNumList[i];
			  if (neighborList[k] == ndx)
			  {
				  dList[i].push_back(1);
			  }
			  else if (find(relatedNPoint[k].begin(), relatedNPoint[k].end(), ndx) != relatedNPoint[k].end())
			  {
				  //wrong
				  double dNumDouble = (double)dNum;
				  double dN = -(1 / dNumDouble);
				  dList[i].push_back(dN);
			  }
			  else
			  {
				  dList[i].push_back(0);
			  }
		  }
	  }

	  //TODO:: Change relatedCPoint into position vector
	  //Multiply the position vector by 1/d
	  //Store it in cList

	  for(int i = 0; i < relatedCPoint.size(); i++)
	  {
		  vector3 sumC(0, 0, 0);
		  for(int j = 0; j < relatedCPoint[i].size(); j++)
		  {
			  int cdx = relatedCPoint[i][j];
			  if(cdx == 0)
			  {
				  sumC = vector3(0, 0, 0);
			  }
			  else
			  {
				  vector3 pt(mesh->vertices[3 * cdx + 0], mesh->vertices[3 * cdx + 1], mesh->vertices[3 * cdx + 2]);
				  //cout << "pt: " << pt.x << ", " << pt.y << ", " << pt.z << endl;
				  sumC = sumC + pt;
			  }
		  }
		  int dNum = neighborNumList[i];
		  sumC = 1 / (double)dNum * sumC;
		  cList.push_back(sumC);
	  }

	  relatedCPoint.clear();
	  relatedNPoint.clear();
	  neighborNumList.clear();

	  for(int i = 0; i < neighborList.size(); i++)
	  {
		  int ndx = neighborList[i];
		  vector3 pt(mesh->vertices[3 * ndx + 0], mesh->vertices[3 * ndx + 1], mesh->vertices[3 * ndx + 2]);
		  vList.push_back(pt);
	  }

	  //TODO::Eigen Solver
	  //Loop through the cList & dList
	  //Put it in the eigen solver
	  //store the answer in vList
	  vector<Triplet<double>> dListEigen;
	  for(int i = 0; i < dList.size(); i++)
	  {
		  for(int j = 0; j < dList[i].size(); j++)
		  {
			  double ddx = dList[i][j];
			  dListEigen.push_back(Triplet<double>(i, j, ddx));
		  }
	  }

	  MatrixXd cListEigen(cList.size(),3);
	  for(int i = 0; i < cList.size(); i++)
	  {
		  cListEigen(i, 0) = cList[i].x;
		  cListEigen(i, 1) = cList[i].y;
		  cListEigen(i, 2) = cList[i].z;
	  }

	  SparseMatrix<double> A(cList.size(), cList.size());
	  A.setFromTriplets(dListEigen.begin(), dListEigen.end());

	  //Convert into ATA=ATb
	  SparseMatrix<double> ATA = A.transpose()*A;
	  cListEigen = A.transpose()*cListEigen;

	  SimplicialCholesky<SparseMatrix<double>> chol(ATA);
	  MatrixXd vListEigen = chol.solve(cListEigen);

	  //TODO::Change vList Eigen to std::Vector
	  //Change the position of the neighbor vertex based on vList vertex
	  for(int i = 0; i < neighborList.size(); i++)
	  {
		  int ndx = neighborList[i];
		  vector3 pt(vListEigen(i, 0), vListEigen(i, 1), vListEigen(i, 2));

		  mesh->vertices[3 * ndx + 0] = vListEigen(i, 0);
		  mesh->vertices[3 * ndx + 1] = vListEigen(i, 1);
		  mesh->vertices[3 * ndx + 2] = vListEigen(i, 2);

		  //cout << "vListEigen: " << pt.x << ", " << pt.y << ", " << pt.z << endl;
	  }

	  //TODO:: Root Squared Mean Distance

	  double sumDist = 0;
	  for (int i = 0; i < vList.size(); i++)
	  {
		  int ndx = neighborList[i];
		  vector3 pt(mesh->vertices[3 * ndx + 0], mesh->vertices[3 * ndx + 1], mesh->vertices[3 * ndx + 2]);
		  double distance;
		  double xDist = pow((pt.x - vList[i].x), 2);
		  double yDist = pow((pt.y - vList[i].y), 2);
		  double zDist = pow((pt.z - vList[i].z), 2);
		  distance = sqrt(xDist + yDist + zDist);
		  sumDist = sumDist + distance;
	  }
	  double rms = sqrt(sumDist / vList.size());
	  cout << "Root Mean Squared Distance = " << rms << endl;

	  //TIMER
	  durationClock = (clock() - startClock) / (double)CLOCKS_PER_SEC;
	  cout << "Reconstruction Time: " << durationClock << " sec" << endl;
  }

  if (button == GLUT_RIGHT_BUTTON && state == GLUT_UP)
	  selectedFeature = -1;

  last_x = x;
  last_y = y;
}

void motion(int x, int y)
{
  tbMotion(x, y);

  /*if (selectedFeature != -1)
  {
	  matrix44 m;
	  vector4 vec = vector4((float)(x - last_x) / 100.0f , (float)(y - last_y) / 100.0f , 0.0 , 1.0);
	  
	  gettbMatrix((float *)&m);
	  vec = m * vec;

	  mesh->vertices[3 * selectedFeature + 0] += vec.x;
	  mesh->vertices[3 * selectedFeature + 1] -= vec.y;
	  mesh->vertices[3 * selectedFeature + 2] += vec.z;
  }*/

  last_x = x;
  last_y = y;
}

void timf(int value)
{
  glutPostRedisplay();
  glutTimerFunc(1, timf, 0);
}

int main(int argc, char *argv[])
{
  WindWidth = 600;
  WindHeight = 600;
	
  GLfloat light_ambient[] = {0.0, 0.0, 0.0, 1.0};
  GLfloat light_diffuse[] = {0.8, 0.8, 0.8, 1.0};
  GLfloat light_specular[] = {1.0, 1.0, 1.0, 1.0};
  GLfloat light_position[] = {0.0, 0.0, 1.0, 0.0};

  glutInit(&argc, argv);
  glutInitWindowSize(WindWidth, WindHeight);
  glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
  glutCreateWindow("Least Square Meshes");

  glutReshapeFunc(Reshape);
  glutDisplayFunc(Display);
  glutMouseFunc(mouse);
  glutMotionFunc(motion);
  glClearColor(0, 0, 0, 0);

  glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
  glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
  glLightfv(GL_LIGHT0, GL_POSITION, light_position);

  glEnable(GL_LIGHT0);
  glDepthFunc(GL_LESS);

  glEnable(GL_DEPTH_TEST);
  glEnable(GL_LIGHTING);
  glEnable(GL_COLOR_MATERIAL);
  tbInit(GLUT_LEFT_BUTTON);
  tbAnimate(GL_TRUE);

  glutTimerFunc(40, timf, 0); // Set up timer for 40ms, about 25 fps

  // load 3D model
  mesh = glmReadOBJ("../data/Dino.obj");
  
  glmUnitize(mesh);
  glmFacetNormals(mesh);
  glmVertexNormals(mesh , 90.0);

  glutMainLoop();

  return 0;

}

